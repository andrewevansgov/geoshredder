var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {


    const getRandomArbitrary = (min, max) => {
        return Math.random() * (max - min) + min;
    }


    const LONDON = [51.509865, -0.118092];

    let randomFeature = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": []
        },
        "properties": {
            "name": "Point minus"
        }
    };


    let geoShredder = { "type": "FeatureCollection",
        "features": []
    };

    for (let i = 0; i < 10000; i++) {
        let lat = getRandomArbitrary(-90, 99);
        let long = getRandomArbitrary(-180, 180);
        let coordinates = [lat, long];

        let tempFeature = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": coordinates
            },
            "properties": {
                "name": "Point minus " + i,
                "mag": getRandomArbitrary(1, 9)
            }
        };

        geoShredder.features.push(tempFeature)
    }

  res.send(JSON.stringify(geoShredder));
});

module.exports = router;
